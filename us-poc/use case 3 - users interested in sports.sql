--city
select city_name, count(user_id_64) as users from (select user_id_64, explode(city_name) as city_name from (select user_id_64, split(city_name,':') as city_name from test where aud_segments='%2650651%')) group by city_name order by users desc limit 10;


--region_name
select region_name, count(user_id_64) as users from (select user_id_64, explode(region_name) as region_name from (select user_id_64, split(region_name,':') as region_name from test  where aud_segments='%2650651%')) group by region_name order by users desc limit 10;

--day
select day, count(user_id_64) as users from (select user_id_64, explode(day) as day from (select user_id_64, split(day,':') as day from test  where aud_segments='%2650651%')) group by day order by users desc limit 10;

--hour
select hour, count(user_id_64) as users from (select user_id_64, explode(hour) as hour from (select user_id_64, split(hour,':') as hour from test   where aud_segments='%2650651%')) group by hour order by users desc limit 10;

--devices
select device_type, count(user_id_64) as users from (select user_id_64, explode(device_type) as device_type from (select user_id_64, split(device_type,':') as device_type from test   where aud_segments='%2650651%')) group by device_type order by users desc limit 10;

--device_makers
select device_make_name, count(user_id_64) as users from (select user_id_64, explode(device_make_name) as device_make_name from (select user_id_64, split(device_make_name,':') as device_make_name from test where    aud_segments='%2650651%')) group by device_make_name order by users desc limit 10;

--device_names
select device_name, count(user_id_64) as users from (select user_id_64, explode(device_name) as device_name from (select user_id_64, split(device_name,':') as device_name from test   where aud_segments='%2650651%')) group by device_name order by users desc limit 10;

-- domains
select site_domain, count(user_id_64) as users from (select user_id_64, explode(site_domain) as site_domain from (select user_id_64, split(site_domain,':') as site_domain from test  where aud_segments='%2650651%')) group by site_domain order by users desc limit 10;

--category of domains
select category,subcategory, count(user_id_64) as users from (select user_id_64, explode(category) as category,explode(subcategory) as subcategory from (select user_id_64, split(category,':') as category, split(subcategory,':') as subcategory from test   where aud_segments='%2650651%')) where category='Automotive' group by category,subcategory order by users desc limit 100;

-- gender
select aud_segments, count(user_id_64) as users from (select user_id_64, explode(aud_segments) as aud_segments from (select user_id_64, split(aud_segments,':') as aud_segments from test where aud_segments='%2650651%')) where aud_segments in (2650558,2650559) group by aud_segments order by users desc limit 10;

--age
select aud_segments, count(user_id_64) as users from (select user_id_64, explode(aud_segments) as aud_segments from (select user_id_64, split(aud_segments,':') as aud_segments from test where  aud_segments='%2650651%')) where aud_segments in (2650524,2650525,2650526,2650527,2650528,2650529) group by aud_segments order by users desc limit 10;

--income
select aud_segments, count(user_id_64) as users from (select user_id_64, explode(aud_segments) as aud_segments from (select user_id_64, split(aud_segments,':') as aud_segments from test where  aud_segments='%2650651%')) where aud_segments in (2650307,2650308,2650309,2650310,2650311,2650312,2650313,2650314) group by aud_segments order by users desc limit 10;


