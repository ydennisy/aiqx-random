--External:
SELECT count(distinct email) FROM aiqx.AIQX_USER_ACTION_INFO where LOGIN_SESSION_ID in (select LOGIN_SESSION_ID from aiqx.AIQX_LOGIN_INFO
where CURRENT_DATE - INTERVAL 90 DAY
and email not like '%@mediaiqdigital.com%'
and email not like '%@regital.com%'
and email not in ('devuser', 'alphauser', 'eiqdemo', 'product-demo', 'testuser', 'product', 'bi-super', 'mediacom'));

SELECT distinct queries FROM aiqx.AIQX_USER_ACTION_INFO where LOGIN_SESSION_ID in (select LOGIN_SESSION_ID from aiqx.AIQX_LOGIN_INFO
where CURRENT_DATE - INTERVAL 90 DAY
and email not like '%@mediaiqdigital.com%'
and email not like '%@regital.com%'
and email not in ('devuser', 'alphauser', 'eiqdemo', 'product-demo', 'testuser', 'product', 'bi-super', 'mediacom'));

--Internal:
SELECT count(distinct email) FROM aiqx.AIQX_USER_ACTION_INFO where LOGIN_SESSION_ID in (select LOGIN_SESSION_ID from aiqx.AIQX_LOGIN_INFO
where CURRENT_DATE - INTERVAL 90 DAY
and email like '%@mediaiqdigital.com%'
or email like '%@regital.com%'
or email in ('devuser', 'alphauser', 'eiqdemo', 'product-demo', 'testuser', 'product', 'bi-super', 'mediacom'))

SELECT distinct queries FROM aiqx.AIQX_USER_ACTION_INFO where LOGIN_SESSION_ID in (select LOGIN_SESSION_ID from aiqx.AIQX_LOGIN_INFO
where CURRENT_DATE - INTERVAL 90 DAY
and email like '%@mediaiqdigital.com%'
or email like '%@regital.com%'
or email in ('devuser', 'alphauser', 'eiqdemo', 'product-demo', 'testuser', 'product', 'bi-super', 'mediacom'))
